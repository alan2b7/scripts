#!/usr/bin/python3
#       _ _
#    __| | |
#   / _` | |
#  | (_| | |
#   \__,_|_|
#
# Description: dl -→- Download contents from a remote
# Homepage:    https://codeberg.org/alan2b7/scripts/src/branch/main/dl.py
# Type:        interactive

import os
import re
import subprocess
import sys


def message_error(message_content, error_code):
    color_red = "\033[1;31m"
    color_reset = "\033[0m"
    print(f"\n [{color_red}x{color_reset}] {message_content}\n")
    sys.exit(error_code)


def message_info(message_content):
    color_blue = "\033[1;34m"
    color_reset = "\033[0m"
    print(f"\n [{color_blue}i{color_reset}] {message_content}")


def message_separator():
    color_blue = "\033[1;34m"
    color_reset = "\033[0m"
    total_columns = subprocess.check_output(['tput', 'cols'], text=True)
    separator = '-' * int(total_columns)
    print(f"{color_blue}{separator}{color_reset}")


def message_spacing(num_lines):
    for _ in range(num_lines):
        print()


def message_success(message_content):
    color_green = "\033[1;32m"
    color_reset = "\033[0m"
    print(f"\n [{color_green}v{color_reset}] {message_content}")


def cfg_set(variable, value, cfg_main):
    """
    Set the host, remote and local directories in the config file
    """
    pattern = rf'^{variable} = "{value}"$'
    key_pattern = rf'^{variable}\b.*$'
    new_entry = f'{variable} = "{value}"'

    with open(cfg_main, 'r+') as file:
        content = file.read()

        if not re.search(pattern, content, re.MULTILINE):
            if re.search(key_pattern, content, re.MULTILINE):
                new_content = re.sub(key_pattern, new_entry,
                                     content, flags=re.MULTILINE)
                file.seek(0)
                file.write(new_content)
                file.truncate()
            else:
                file.write(new_entry + '\n')


def select_mode(cmd_fzf):
    # Shallow copy to not modify the original list outside this function
    cmd_fzf = cmd_fzf.copy()
    cmd_fzf.append("--prompt=🛸 Select mode: ")

    process_fzf = subprocess.Popen(cmd_fzf,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   text=True)

    modes = [
        "flac\t\t→\tSearch for .flac directories, sorted by date\n"
        "mkv\t\t→\tSearch for .mkv file(s), sorted by date\n"
        "dirs\t\t→\tSearch for any directories\n"
        "mp4\t\t→\tSearch for .mp4 file(s), sorted by date\n"
        "reset\t\t→\tReset configuration\n"
        "host\t\t→\tSwitch remote host\n"
        "resume\t\t→\tResume previous download\n"
    ]

    selected_line, _ = process_fzf.communicate('\n'.join(modes))

    if process_fzf.returncode == 0:
        selected_line = selected_line.strip()
        mode = selected_line.split('\t')[0]
        return mode
    else:
        message_error("Download mode not selected", 10)


def select_host(cmd_fzf):
    # Shallow copy to not modify the original list outside this function
    cmd_fzf = cmd_fzf.copy()
    cmd_fzf.append("--prompt=🛸 Select host: ")

    process_fzf = subprocess.Popen(cmd_fzf,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   text=True)

    cfg_ssh = os.path.expanduser("~/.ssh/config")
    with open(cfg_ssh, 'r') as file:
        content = file.read()

    hosts = re.findall(r'^Host\s+([^\s]+)', content, re.MULTILINE)
    hosts = [host
             for host in hosts
             if not re.search(r'codeberg|git|tunnel', host)]

    selected_host, _ = process_fzf.communicate('\n'.join(hosts))
    return selected_host.strip()


def select_directory(origin, mode, cmd_fzf, remote_host=None):
    if origin == "remote":
        cmd_find = [
            "ssh", "-q", remote_host,
            "find /home -mindepth 2 -maxdepth 5 "
            "\\( -iname '.*' -type d -prune \\) -o -type d -print0"
        ]
        cmd_fzf_opts = [
            f"--prompt=🛸 Select {mode} remote source: "
        ]
    elif origin == "local":
        cmd_find = [
            "find",
            os.environ['HOME'],
            f"/media/{os.environ['USER']}",
            "/mnt",
            f"/run/media/{os.environ['USER']}",
            "-mindepth", "1", "-maxdepth", "1",
            "(", "-iname", ".*", "-type", "d", "-prune", ")",
            "-o", "-user", os.environ['USER'],
            "-type", "d", "-print0"
        ]
        cmd_fzf_opts = [
            f"--prompt=🛸 Select {mode} local destination: "
        ]

    # Shallow copy to not modify the original list outside this function
    cmd_fzf = cmd_fzf.copy()
    cmd_fzf.append("--read0")
    cmd_fzf.extend(cmd_fzf_opts)

    process_find = subprocess.Popen(cmd_find,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.DEVNULL,
                                    text=True)

    process_fzf = subprocess.Popen(cmd_fzf,
                                   stdin=process_find.stdout,
                                   stdout=subprocess.PIPE,
                                   text=True)

    process_find.stdout.close()

    selected_directory, _ = process_fzf.communicate()

    return selected_directory.strip()


def select_items(cmd_find, cmd_fzf, download_list):
    process_find = subprocess.Popen(cmd_find,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.DEVNULL,
                                    text=True)

    process_fzf = subprocess.Popen(cmd_fzf,
                                   stdin=process_find.stdout,
                                   stdout=subprocess.PIPE,
                                   text=True)

    process_find.stdout.close()

    selected_items, _ = process_fzf.communicate()

    if process_fzf.returncode == 0:
        selected_items = selected_items.strip()
        with open(download_list, 'w') as file:
            file.write(selected_items)
    else:
        message_error("Nothing selected for download", 60)


def return_cmd_find(mode, remote_host, remote_source):
    if mode in ("mkv", "mp4"):
        return [
            "ssh", "-q", f"{remote_host}",
            f"find '{remote_source}' -mindepth 1 -maxdepth 5 "
            f"-iname '*.{mode}' -type f -printf '%T@#%p\n' | "
            "sort -r | "
            "cut -d# -f2-"
        ]
    elif mode in ("dirs", "flac"):
        return [
            "ssh", "-q", f"{remote_host}",
            f"find '{remote_source}' -mindepth 1 -maxdepth 2 "
            f"-type d -printf '%T@#%p\n' | "
            "sort -r | "
            "cut -d# -f2-"
        ]


def download_stuff(cmd_rsync, dry_run=None):
    if dry_run is True:
        message_info("DRY RUN")
        message_separator()
        cmd_rsync = cmd_rsync.copy()
        cmd_rsync.append("--dry-run")

        for i, item in enumerate(cmd_rsync):
            if '--info=name,progress2,stats2' in item:
                cmd_rsync[i] = '--info=name,stats2'

    process_rsync = subprocess.Popen(cmd_rsync,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE,
                                     text=True)

    try:
        for line in process_rsync.stdout:
            if dry_run is True:
                sys.stdout.write(line)

            status_line = re.search(
                r'^\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)', line)
            if status_line:
                size = status_line.group(1)
                percentage = status_line.group(2)
                speed = status_line.group(3)
                time = status_line.group(4)
                print(f"     {percentage} downloaded ({size})"
                      f" at {speed}, ETA: {time}"
                      f"{' ' * 10}",
                      end='\r',
                      flush=True)

        return_code = process_rsync.wait()

        if return_code == 0:
            if dry_run is False:
                message_success("Done!")
                message_spacing(1)
        else:
            message_error("The ship ran aground", return_code)

    except KeyboardInterrupt:
        process_rsync.terminate()
        message_spacing(1)
        message_info("Download cancelled")
        message_spacing(1)


def main():
    dl_version = "1.3"

    cfg_dir = os.path.expanduser("~/.config/dl")
    cfg_main = os.path.join(cfg_dir, "main.py")
    cfg_resume = os.path.join(cfg_dir, "resume.py")
    download_list = os.path.join(cfg_dir, "list.txt")

    # Fallback if $colorful is not set in the shell environment
    colorful = os.getenv("colorful", "#00afd7")

    cmd_fzf = [
        'fzf',
        '--cycle',
        '--layout=reverse',
        f'--color=bg+:#0d0d0d,gutter:-1,hl:bold,hl:{colorful},'
        f'fg+:bold,fg+:#ffffff,hl+:bold,hl+:{colorful},info:{colorful},'
        f'border:{colorful},separator:{colorful},scrollbar:{colorful},'
        f'prompt:{colorful},pointer:{colorful},'
        f'marker:{colorful},header:#999999'
    ]

    mode = select_mode(cmd_fzf)

    if mode == "reset":
        if os.path.exists(cfg_main):
            os.remove(cfg_main)
        mode = select_mode(cmd_fzf)

    # Setup main configuration file if needed
    if not os.path.exists(cfg_main):
        os.makedirs(cfg_dir, mode=0o700, exist_ok=True)
        content = f"# ~/.config/dl/main.py - dl {dl_version}\n\n"
        with open(cfg_main, 'w') as file:
            file.write(content)

    # Load main configuration
    cfg_main_content = {}

    with open(cfg_main, 'r') as file:
        exec(file.read(), cfg_main_content)

    # Configure where to connect with SSH
    remote_host = cfg_main_content.get("remote_host", None)

    if not remote_host or mode == "host":
        remote_host = select_host(cmd_fzf)

        if mode == "host":
            mode = select_mode(cmd_fzf)

        if remote_host:
            cfg_set("remote_host", remote_host, cfg_main)
        else:
            message_error("No remote host selected", 11)

    # Configure which remote directory to use as source
    configured_remote_source = f"{remote_host}_{mode}_remote_source"
    remote_source = cfg_main_content.get(configured_remote_source, None)

    if not remote_source:
        remote_source = select_directory("remote", mode, cmd_fzf, remote_host)
        if remote_source:
            cfg_set(configured_remote_source, remote_source, cfg_main)
        else:
            message_error("Remote source directory not configured", 12)

    # Configure the local directory to use as destination
    configured_local_destination = f"{remote_host}_{mode}_local_destination"
    local_destination = cfg_main_content.get(
        configured_local_destination, None)

    if not local_destination:
        local_destination = select_directory("local", mode, cmd_fzf)
        if local_destination:
            cfg_set(configured_local_destination, local_destination, cfg_main)
        else:
            message_error("Local destination directory not configured", 13)

    # Create the download list file
    cmd_find = return_cmd_find(mode, remote_host, remote_source)

    cmd_fzf_opts = [
        '--multi',
        '--delimiter=/',
        '--with-nth=-1',
        f'--prompt=🛸 Download {mode}: '
    ]

    cmd_fzf.extend(cmd_fzf_opts)

    select_items(cmd_find, cmd_fzf, download_list)

    # Setup the rsync command
    cmd_rsync = [
        'rsync',
        '-avh',
        '--info=name,progress2,stats2',
        '--no-relative',
        '--partial',
        f'--files-from={download_list}',
        f'{remote_host}:/',
        f'{local_destination}'
    ]

    if mode == "flac":
        cmd_rsync.append("--recursive")

    # Setup for resuming the download
    cfg_resume_content = (
        f"# ~/.config/dl/resume.py - dl {dl_version}\n\n"
        f"cmd_rsync = {cmd_rsync}"
    )

    with open(cfg_resume, 'w') as file:
        file.write(cfg_resume_content)

    # Start the download
    download_stuff(cmd_rsync, dry_run=True)

    message_info(f"Local destination: {local_destination}")
    message_separator()
    message_spacing(1)

    confirmation = input(" [\033[1;34mi\033[0m] Is this OK? (y/n) ").lower()

    if confirmation == 'y':
        message_separator()
        download_stuff(cmd_rsync, dry_run=False)
    else:
        message_info("Not starting the download")
        message_spacing(1)


if __name__ == '__main__':
    main()
