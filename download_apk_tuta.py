#!/usr/bin/python3
# Download the latest Tuta .apk release

import requests
import re
import os
import hashlib

releases_url = "https://api.github.com/repos/tutao/tutanota/releases"
response_api = requests.get(releases_url)

if response_api.status_code == 200:
    data = response_api.json()
    latest_url = None
    checksum = None

    for release in data:
        for asset in release.get('assets', []):
            asset_url = asset.get('browser_download_url')
            if asset_url and '/tutanota-app' in asset_url:
                # Update the latest URL if the version in the current URL
                # is greater than the one previously stored
                # filename: "tutanota-app-tutao-release-250.241025.0.apk"
                if not latest_url or asset_url > latest_url:
                    latest_url = asset_url

                    # Find the sha256 checksum value in "body"
                    body = release.get('body', '')
                    checksum_match = re.search(r'\b[a-f0-9]{64}\b', body)
                    if checksum_match:
                        checksum = checksum_match.group(0)

                    html_url = release.get('html_url', '')

    response = requests.get(latest_url, stream=True)

    if response.status_code == 200:
        basename = os.path.basename(latest_url)
        downloads = os.path.expanduser("~/downloads")
        filename = os.path.join(downloads, basename)

        sha256 = hashlib.sha256()
        with open(filename, 'wb') as out_file:
            for chunk in response.iter_content(chunk_size=8192):
                out_file.write(chunk)
                sha256.update(chunk)

        downloaded_checksum = sha256.hexdigest()

        if downloaded_checksum == checksum:
            print(f"{html_url}")
            print(f"{checksum} (release checksum)")
            print(f"{downloaded_checksum} (downloaded checksum)")
            print("Checksum matches!")
        else:
            print("Checksum does not match")
    else:
        print(f"Error downloading APK file: {response.status_code}")
else:
    print(f"Error downloading API: {response_api.status_code}")
