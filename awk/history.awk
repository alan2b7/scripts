#!/usr/bin/awk -f
# Print in reverse order ~/.bash_history and keep only recent duplicates

{
    gsub(/[[:space:]]+$/, "");
    lines[NR]=$0
}
END {
    for (i=NR; i>=1; i--)
        if (!seen[lines[i]]++)
            print lines[i]
}
